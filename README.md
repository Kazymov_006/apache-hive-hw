```
    docker-compose up -d
```

## Testing
Load data into Hive:
```
  $ docker exec -it dwh2-namenode-1 bash
  # hdfs dfs -put twcs.csv /user/hive/warehouse/data.csv 
  
  $ docker-compose exec hive-server bash
  # /opt/hive/bin/beeline -u jdbc:hive2://localhost:10000
  > CREATE TABLE tweets (tweet_id INT, author_id INT, inbound BOOLEAN, created_at STRING, text STRING, response_tweet_id INT, in_response_to_tweet_id INT) row format delimited fields terminated by ',';
  > LOAD DATA INPATH '/user/hive/warehouse/data.csv' OVERWRITE INTO TABLE tweets;
  > CREATE TABLE word_cnt AS (SELECT word, COUNT(*) cnt FROM tweets LATERAL VIEW explode(split(text, ' ')) lTable as word GROUP BY word ORDER BY cnt DESC);
```